use std::collections::HashMap;
use std::time::Duration;
use consts::{
    KEYCODE_ENTER,
    KEYCODE_TAB,
    KEYCODE_DEL,
};
use error::SimpleError;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Action {
    None,
    Quit,
    Enter,
    Tab,
    Clear,
    ListMacros,
    Secret,
    Load(String),
    Save(String),
    SendText(String),
    Wait(u64),
    Delete(u32),
    RunMacro(String),
    DefineMacro(String, Vec<Action>),
    ExportMacros(String, String),
}

fn keyevent(evt: &'static str, cnt: u32) -> String {
    if cnt == 0 {
        format!("adb shell input keyevent {}\n", evt)
    } else {
        let mut s = String::from("adb shell input keyevent ");
        for _ in 0..cnt {
            s.push_str(evt);
            s.push(' ');
        }
        s.push('\n');
        s
    }
}

fn expand_wait_to_bash(t: u64) -> String {
    let ms = Duration::from_millis(t);
    let pysleep = ms.as_secs() as f64
        + ms.subsec_millis() as f64 * 1e-3;
    format!("python -c 'import time;time.sleep({})'\n", pysleep)
}

fn expand_macro_to_bash(mac: &String, macros: &HashMap<String, Vec<Action>>) -> Result<String, SimpleError> {
    let acts = macros.get(mac);
    if acts.is_none() {
        return Err(SimpleError::from(format!("macro {} doesn't exist", mac)));
    }
    let mut s = String::new();
    for a in acts.unwrap().into_iter() {
        s.push_str(&a.to_bash_cmd(macros)?);
    }
    Ok(s)
}

impl Action {
    pub fn to_bash_cmd(&self, macros: &HashMap<String, Vec<Action>>) -> Result<String, SimpleError> {
        match *self {
            Action::Tab => Ok(keyevent(KEYCODE_TAB, 1)),
            Action::Enter => Ok(keyevent(KEYCODE_ENTER, 1)),
            Action::Delete(n) => Ok(keyevent(KEYCODE_DEL, n)),
            Action::Wait(n) => Ok(expand_wait_to_bash(n)),
            Action::SendText(ref s) => Ok(format!("adb shell input text '{}'\n", s)),
            Action::RunMacro(ref mac) => expand_macro_to_bash(mac, macros),
            _ => {
            return Err(SimpleError::from("action {:?} doesn't have a bash counterpart"));
            }
        }
    }
}
