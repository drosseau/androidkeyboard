use std::{str};
use std::error::Error;

use error::SimpleError;

macro_rules! ret_err {
    ($e:expr) => {
        return Err(Box::new(SimpleError::from($e)));
    }
}

extern crate pancurses;

use pancurses::{
    Input,
    ERR,
    Window,
};

const BANNER: &'static str = r#"================================================================================
=                             Android ADB Keyboard                             =
=                                                                              =
=                                                                              =
=                To end your session just type \q and hit enter                =
=                                                                              =
=                        Hit F1 to enter multiline mode                        =
=                                                                              =
=                                                                              =
================================================================================

"#;

const PROMPT: &'static str = "> ";
// These aren't actually showing up well on Linux :(
const TAB_ECHO: &'static str = " ➜ ";
const ENTER_ECHO: &'static str = "↵\n";

macro_rules! write_str_ret {
    ($win:expr, $s:expr) => {
        if $win.addstr($s) == ERR {
            ret_err!(format!("\ncurses error writing string: {}", $s));
        }
    }
}

macro_rules! write_char_ret {
    ($win:expr, $c:expr) => {
        if $win.addch($c) == ERR {
            ret_err!(format!("\ncurses error writing char: {}", $c));
        }
    }
}

#[derive(Debug)]
enum State {
    Normal,
    MultiLine,
}

#[derive(Debug)]
pub(crate) struct Editor<'a> {
    cmd_buf: Vec<u8>,
    cmd_buf_widths: Vec<u8>,
    echo_buf: Vec<u8>,
    echo_buf_widths: Vec<u8>,
    state: State,
    prompt: String,
    len: usize,
    win: &'a Window,
    start_y: i32,
    new_prompt: bool,
    secret: bool,
}

impl<'a> Editor<'a> {
    pub(crate) fn new(win: &'a Window, pre: &Option<String>) -> Self{
        win.keypad(true);
        let mut prompt = String::new();
        if let Some(ref s) = *pre {
            prompt.push_str(&s);
        }
        let y = win.get_cur_y();
        prompt.push_str(PROMPT);
        Self{
            cmd_buf: Vec::with_capacity(512),
            echo_buf: Vec::with_capacity(512),
            cmd_buf_widths: Vec::with_capacity(512),
            echo_buf_widths: Vec::with_capacity(512),
            prompt: prompt,
            win: win,
            new_prompt: true,
            start_y: y,
            state: State::Normal,
            len: 0,
            secret: false,
        }
    }

    pub(crate) fn toggle_secret(&mut self) {
        self.secret = !self.secret;
    }

    pub(crate) fn clear_line(&mut self) {
        let  y = self.win.get_cur_y();
        for i in (self.start_y..y+1).rev() {
            self.win.mv(i, 0);
            self.win.clrtoeol();
        }
    }

    pub(crate) fn write_banner(&mut self) {
        self.win.addstr(BANNER);
        self.start_y = self.win.get_cur_y();
    }

    pub(crate) fn new_line(&mut self) -> Result<(), Box<Error>> {
        write_char_ret!(self.win, '\n');
        self.start_y = self.win.get_cur_y();
        self.write_prompt()
    }

    pub(crate) fn write_str_ln(&mut self, s: &str) -> Result<(), Box<Error>> {
        write_str_ret!(self.win, s);
        write_char_ret!(self.win, '\n');
        self.write_prompt()
    }

    pub(crate) fn write_prompt(&mut self) -> Result<(), Box<Error>> {
        match self.state {
            State::Normal => write_str_ret!(self.win, &self.prompt),
            State::MultiLine => write_str_ret!(self.win, ">>> "),
        };
        Ok(())
    }

    fn handle_backspace(&mut self) -> Result<(), Box<Error>> {
        if self.len == 0 {
            return Ok(());
        }
        match self.cmd_buf_widths.pop() {
            Some(n) => {
                for _ in 0..n {
                    self.cmd_buf.pop();
                }
            },
            None => {},
        };
        match self.echo_buf_widths.pop() {
            Some(n) => {
                for _ in 0..n {
                    self.echo_buf.pop();
                }
            },
            None => {},
        };
        self.clear_line();
        match self.state {
            State::Normal => {
                self.write_prompt()?;
                write_str_ret!(self.win, str::from_utf8(&self.echo_buf)?);
            },
            State::MultiLine => {
                write_str_ret!(self.win, &self.prompt);
                let split: Vec<&str> = str::from_utf8(&self.echo_buf)?.split('\n').collect();
                for i in 0..split.len()  {
                    write_str_ret!(self.win, split[i]);
                    if i < split.len() - 1 {
                        write_char_ret!(self.win, '\n');
                        write_str_ret!(self.win, ">>> ");
                    }
                }
            },
        };
        Ok(())
    }

    fn handle_char(&mut self, c: char) -> Result<bool, Box<Error>> {
        match c {
            '\n' => {
                match self.state {
                    State::Normal => {
                        if self.len == 0 {
                            self.new_prompt = false;
                            self.add_to_buf(c, false)?;
                        }
                        Ok(true)
                    },
                    State::MultiLine => {
                        self.add_to_buf(c, true)?;
                        self.write_prompt()?;
                        Ok(false)
                    }
                }
            }
            '\t' => {
                match self.state {
                    State::Normal => {
                        if self.len == 0 {
                            self.new_prompt = false;
                            self.add_to_buf(c, false)?;
                        } else {
                            self.add_to_buf(c, true)?;
                        }
                        Ok(true)
                    },
                    State::MultiLine => {
                        self.add_to_buf(c, true)?;
                        Ok(false)
                    }
                }
            },
            '\u{007f}' | '\u{0008}' => {
                if self.echo_buf.len() == 0 {
                    self.new_prompt = false;
                    self.add_to_buf(c, false)?;
                    Ok(true)
                } else {
                    self.handle_backspace()?;
                    Ok(false)
                }
            },
            _ => {
                self.add_to_buf(c, true)?;
                Ok(false)
            },
        }
    }

    fn pushc_echo(&mut self, c: char, echo: bool) -> Result<(), Box<Error>> {
        if self.secret {
            self.echo_buf.push(b'*');
            self.echo_buf_widths.push(1);
            if echo {
                write_char_ret!(self.win, '*');
            }
        } else {
            let mut into = [0; 4];
            let s = c.encode_utf8(&mut into);
            self.pushs_echo(s, echo)?;
        }
        Ok(())
    }

    fn pushs_echo(&mut self, s: &str, echo: bool) -> Result<(), Box<Error>> {
        if self.secret {
            for _ in 0..s.len() {
                self.pushc_echo('*', echo)?;
            }
        } else {
            self.echo_buf_widths.push(s.len() as u8);
            for b in s.as_bytes() {
                self.echo_buf.push(*b);
            }
            if echo {
                write_str_ret!(self.win, s);
            }
        }
        Ok(())
    }

    fn add_to_buf(&mut self, c: char, echo: bool) -> Result<(), Box<Error>> {
        match c {
            '\'' => {
                self.len += 1;
                self.pushc_echo(c, echo)?;
                let prev = self.cmd_buf.len() as u8;
                self.cmd_buf.push(b'\'');
                self.cmd_buf.push(b'"');
                self.cmd_buf.push(b'\'');
                self.cmd_buf.push(b'"');
                self.cmd_buf.push(b'\'');
                self.cmd_buf_widths.push(self.cmd_buf.len() as u8 - prev);
            },
            '\n' => {
                self.len += 1;
                self.pushs_echo(ENTER_ECHO, echo)?;
                self.cmd_buf.push(b'\n');
                self.cmd_buf_widths.push(1);
            },
            '\t' => {
                self.len += 1;
                self.pushs_echo(TAB_ECHO, echo)?;
                self.cmd_buf.push(b'\t');
                self.cmd_buf_widths.push(1);
            },
            _ => {
                self.pushc_echo(c, echo)?;
                let mut into = [0; 4];
                let s = (c as char).encode_utf8(&mut into);
                self.cmd_buf_widths.push(s.len() as u8);
                for b in s.as_bytes() {
                    self.len += 1;
                    self.cmd_buf.push(*b);
                }
            },
        };
        Ok(())
    }

    pub(crate) fn get_line(&mut self) -> Result<Vec<u8>, Box<Error>> {
        self.echo_buf.truncate(0);
        self.cmd_buf.truncate(0);
        self.len = 0;
        if self.new_prompt {
            self.new_line()?;
        } else {
            self.new_prompt = true;
        }
        loop {
            match self.win.getch() {
                Some(Input::KeyBackspace) => self.handle_backspace()?,
                Some(Input::KeyF1) => {
                    match self.state {
                        State::Normal => self.state = State::MultiLine,
                        State::MultiLine => self.state = State::Normal,
                    };
                },
                Some(Input::Character(c)) => {
                    if self.handle_char(c)? {
                        return Ok(self.cmd_buf.clone());
                    }
                },
                Some(Input::Unknown(c)) => {
                    let msg = &format!("Unknown character `{}`", c);
                    self.write_str_ln(msg)?;
                },
                Some(_) => {
                    // Don't do anything with unrecognized input
                },
                None => (),
            };
        }
    }
}
