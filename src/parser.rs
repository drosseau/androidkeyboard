use std::{str, fmt, u64};
use std::str::FromStr;
use std::error::{Error};

use action::Action;

const MAX_TEXT_BLOCK: usize = 256;

#[derive(Debug)]
enum Cmd {
    Quit,
    Save,
    Load,
    Clear,
    Wait,
    Delete,
    RunMacro,
    DefineMacro,
    ListMacros,
    Export,
    Secret,
    Unknown,
}

impl<'a> From<&'a str> for Cmd {
    fn from(s: &'a str) -> Cmd {
        // Allow for using `\ma` for `\macros` or `\c` for `\clear` etc
        match s.as_bytes()[0] {
            b'd' => Cmd::Delete,
            b'q' => Cmd::Quit,
            b'm' => if s.len() == 1 {
                Cmd::RunMacro
            } else {
                match s.as_bytes()[1] {
                    b'd' => Cmd::DefineMacro,
                    b'a' => Cmd::ListMacros,
                    _ => Cmd::Unknown
                }
            },
            b'p' => Cmd::Secret,
            //"mdef" => Cmd::DefineMacro,
            //"macros" => Cmd::ListMacros,
            b'c' => Cmd::Clear,
            //"clear" => Cmd::Clear,
            b'w' => Cmd::Wait,
            b's' => Cmd::Save,
            b'l' => Cmd::Load,
            b'e' => Cmd::Export,
            //"wait" => Cmd::Wait,
            //"save" => Cmd::Save,
            //"load" => Cmd::Load,
            _ => Cmd::Unknown,
        }
    }
}

#[derive(Debug)]
pub(crate) struct ParseError {
    desc: String,
}

impl ParseError {
    fn bad_input(cmd: &str, input: &str) -> Self {
        Self{
            desc: format!("bad input for command {}: {}", cmd, input),
        }
    }

    fn unknown_cmd(cmd: &str) -> Self {
        Self{
            desc: format!("unknown command: {}", cmd),
        }
    }

    fn generic(s: &str) -> Self {
        Self{
            desc: String::from(s),
        }
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for ParseError {
    fn description(&self) -> &str {
        &self.desc
    }

    fn cause(&self) -> Option<&Error> {
        None
    }
}


#[derive(Debug)]
pub(crate) struct Parser<'a> {
    buf: &'a [u8],
    pos: usize,
    len: usize,
    broke_text: bool,
}

macro_rules! from_utf8 {
    ($i:expr, $j:expr, $buf:expr) => {
        match str::from_utf8(&$buf[$i..$j]) {
            Err(_) => return Err(ParseError::generic("failed to convert bytes from utf8")),
            Ok(s) => s,
        }
    }
}

impl<'a> Parser<'a> {
    pub(crate) fn new(s: &'a [u8]) -> Self {
        let len = s.len();
        Self{
            pos: 0,
            buf: s,
            len: len,
            broke_text: false,
        }
    }

    fn parse_command(&mut self, v: &mut Vec<Action>) -> Result<(), ParseError> {
        let i = self.move_to_next_word();
        let s = from_utf8!(i, self.pos, self.buf);
        match Cmd::from(s) {
            Cmd::Secret => { v.push(Action::Secret); Ok(()) },
            Cmd::Unknown => Err(ParseError::unknown_cmd(s)),
            Cmd::Quit => { v.push(Action::Quit); Ok(()) },
            Cmd::Clear => { v.push(Action::Clear); Ok(()) },
            Cmd::ListMacros => { v.push(Action::ListMacros); Ok(()) },
            Cmd::Delete => self.parse_delete(v),
            Cmd::Wait => self.parse_wait(v),
            Cmd::DefineMacro => self.parse_macro_definition(v),
            Cmd::RunMacro => self.parse_macro_run(v),
            Cmd::Save => self.parse_save(v),
            Cmd::Load => self.parse_load(v),
            Cmd::Export => self.parse_export(v),
        }
    }

    fn parse_export(&mut self, v: &mut Vec<Action>) -> Result<(), ParseError> {
        self.consume_whitespace();
        let start = self.move_to_next_word();
        let mac = String::from(from_utf8!(start, self.pos, self.buf));
        v.push(Action::ExportMacros(mac.clone(), format!("{}.akexport.sh", mac.clone())));
        self.pos += 1;
        self.consume_newline();
        Ok(())
    }

    fn parse_load(&mut self, v: &mut Vec<Action>) -> Result<(), ParseError> {
        self.consume_whitespace();
        let start = self.move_to_next_word();
        let fname = String::from(from_utf8!(start, self.pos, self.buf));
        v.push(Action::Load(fname));
        self.pos += 1;
        self.consume_newline();
        Ok(())
    }

    fn parse_save(&mut self, v: &mut Vec<Action>) -> Result<(), ParseError> {
        self.consume_whitespace();
        let start = self.move_to_next_word();
        let fname = String::from(from_utf8!(start, self.pos, self.buf));
        v.push(Action::Save(fname));
        self.pos += 1;
        self.consume_newline();
        Ok(())
    }

    // consumes all whitespace starting from self.pos
    fn consume_whitespace(&mut self) {
        while self.pos < self.len && self.buf[self.pos] == b' ' {
            self.pos += 1;
        }
    }

    fn read_number<T: FromStr>(&mut self, cmd: &str) -> Result<T, ParseError> {
        self.consume_whitespace();
        let start = self.move_to_next_word();
        let arg = from_utf8!(start, self.pos, self.buf);
        if arg == "\n" || start == self.len {
            return Err(ParseError::bad_input("d", "no arguments given"));
        }
        let count = match T::from_str(arg) {
            Ok(v) => v,
            Err(_) => {
                let msg = format!("`{}` is not valid input", arg);
                return Err(ParseError::bad_input(&String::from(cmd), &msg));
            },
        };
        Ok(count)
    }

    fn parse_delete(&mut self, v: &mut Vec<Action>) -> Result<(), ParseError> {
        let count = self.read_number::<u32>("d")?;
        if count > 255 {
            let msg = format!("`{}` is larger than the max delete count of 255", count);
            return Err(ParseError::bad_input("d", &msg));
        }
        self.pos += 1;
        self.consume_newline();
        v.push(Action::Delete(count));
        Ok(())
    }

    fn parse_wait(&mut self, v: &mut Vec<Action>) -> Result<(), ParseError> {
        let count = self.read_number::<u64>("wait")?;
        self.pos += 1;
        self.consume_newline();
        v.push(Action::Wait(count));
        Ok(())
    }

    fn consume_newline(&mut self) {
        if self.pos < self.len && self.buf[self.pos] == b'\n' {
            self.pos += 1;
        }
    }

    // returns a value i such that i..self.pos is the next word
    fn move_to_next_word(&mut self) -> usize {
        let i = self.pos;
        while self.pos < self.len && self.buf[self.pos] != b' ' && self.buf[self.pos] != b'\n' {
            self.pos += 1;
        }
        i
    }

    fn parse_macro_definition(&mut self, v: &mut Vec<Action>) -> Result<(), ParseError> {
        self.consume_whitespace();
        let mut start = self.move_to_next_word();
        let name = String::from(from_utf8!(start, self.pos, self.buf));
        // make sure that was all we had on the line
        while self.pos < self.len && self.buf[self.pos] != b'\n' {
            if self.buf[self.pos] != b' ' {
                return Err(ParseError::bad_input("mdef", "first line must only be \\mdef NAME"));
            }
            self.pos += 1;
        }
        self.consume_newline();
        start = self.pos;
        let mut end = self.pos;
        let mut actions = Vec::new();
        while end < self.len {
            match self.buf[end] {
                b'\n' => {
                    // Single newline
                    if start == end {
                        actions.push(Action::Enter);
                    } else {
                        Parser::new(&self.buf[start..end]).parse(&mut actions)?;
                        let mut push_enter = false;
                        match actions.iter().last() {
                            Some(&Action::RunMacro(ref mac)) => {
                                if *mac == name {
                                    return Err(
                                        ParseError::bad_input("mdef", "cannot handle recursive macros")
                                    );
                                }
                            },
                            Some(&Action::SendText(_)) | Some(&Action::Enter) | Some(&Action::Tab) => {
                                push_enter = true;
                            },
                            _ => {},
                        };
                        if push_enter {
                            actions.push(Action::Enter);
                        }
                    }
                    end += 1;
                    start = end;
                },
                b'\t' => {
                    if end - start > 1 {
                        Parser::new(&self.buf[start..end]).parse(&mut actions)?;
                    }
                    actions.push(Action::Tab);
                    end += 1;
                    start = end;
                },
                _ => end += 1,
            };
        }
        if start != end {
            Parser::new(&self.buf[start..end]).parse(&mut actions)?;
        }
        match actions.iter().last() {
            Some(&Action::RunMacro(ref mac)) => {
                if *mac == name {
                    return Err(
                        ParseError::bad_input("mdef", "cannot handle recursive macros")
                    );
                }
            },
            _ => (),
        };
        v.push(Action::DefineMacro(name, actions));
        self.pos = end;
        Ok(())
    }

    fn parse_macro_run(&mut self, v: &mut Vec<Action>) -> Result<(), ParseError> {
        self.consume_whitespace();
        let start = self.move_to_next_word();
        if start == self.len {
            return Err(ParseError::bad_input("m", "no macro given"));
        }
        let name = String::from(from_utf8!(start, self.pos, self.buf));
        v.push(Action::RunMacro(name));
        self.pos += 1;
        self.consume_newline();
        Ok(())
    }

    // returns a value i such that i..self.pos is the next line
    /*
    fn find_end_of_line(&mut self) -> usize {
        let i = self.pos;
        while self.pos < self.len && self.buf[self.pos] != b'\n' {
            self.pos += 1;
        }
        i
    }
    */

    fn just_text(&mut self, v: &mut Vec<Action>) -> Result<(), ParseError> {
        let start = self.pos;
        let mut count = 0;
        while
            self.pos < self.len &&
            count < MAX_TEXT_BLOCK &&
            self.buf[self.pos] != b'\n' &&
            self.buf[self.pos] != b'\t'
        {
            self.pos += 1;
            count += 1;
        }
        if count == MAX_TEXT_BLOCK {
            self.broke_text = true;
        } else {
            self.broke_text = false;
            if count == 0 {
                return Ok(());
            }
        }
        let text = from_utf8!(start, self.pos, self.buf);
        v.push(Action::SendText(String::from(text)));
        Ok(())
    }

    pub(crate) fn parse(&mut self, v: &mut Vec<Action>) -> Result<(), ParseError> {
        if self.len == 0 {
            v.push(Action::None);
        } else if self.len == 1 {
            match self.buf[0] {
                0x7f | 0x08 => v.push(Action::Delete(1)),
                b'\n' => v.push(Action::Enter),
                b'\t' => v.push(Action::Tab),
                b'\\' => return Err(
                    ParseError::generic(
                        "first character \\'s must be escaped or precede a command"
                    )),
                _ => self.just_text(v)?,
            }
        } else {
            let i = 0;
            while self.pos < self.len {
                if self.broke_text {
                    self.just_text(v)?;
                } else {
                    match self.buf[self.pos] {
                        0x7f | 0x08 => { v.push(Action::Delete(1)); self.pos += 1; },
                        b'\n' => { v.push(Action::Enter); self.pos += 1; },
                        b'\t' => { v.push(Action::Tab); self.pos += 1; },
                        b'\\' => {
                            if self.pos + 1 >= self.len {
                                return Err(
                                    ParseError::generic(
                                        "first character \\'s must be escaped or precede a command"
                                ));
                            }
                            if self.buf[i + 1] != b'\\' {
                                self.pos += 1;
                                self.parse_command(v)?;
                            } else {
                                self.pos += 1;
                                self.just_text(v)?;
                            }
                        },
                        _ => {
                            self.just_text(v)?;
                        },
                    };
                }
            }
        };
        Ok(())
    }
}

#[cfg(test)]
#[macro_use]
pub(crate) mod test {
    use super::*;

    macro_rules! do_parse {
        ($buf:expr, $c:expr, ok) => {{
            let mut a = Vec::new();
            let mut parser = Parser::new(&$buf);
            let res = parser.parse(&mut a);
            assert!(res.is_ok(), "Expected Ok but got {:?}", res.err());
            assert!(
                a.len() == $c,
                "Expected len {} but got {}: {:?}", $c, a.len(), a
            );
            a
        }};
        ($buf:expr, $c:expr, err) => {{
            let mut a = Vec::new();
            let mut parser = Parser::new(&$buf);
            let res = parser.parse(&mut a);
            assert!(
                res.is_err(),
                "Expected an error but got: {:?}", res
            );
            assert!(
                a.len() == $c,
                "Expected len {} but got {}: {:?}", $c, a.len(), a
            );
            a
        }};
    }

    macro_rules! expect_sendtext {
        ($a:expr, $txt:expr) => {
            assert!(match $a {
                Action::SendText(ref t) => t == $txt,
                _ => false,
            },
            "Expected Action::SendText(\"{}\") but got {:?}", $txt, $a
            );
        }
    }

    macro_rules! expect_runmacro {
        ($a:expr, $txt:expr) => {
            assert!(match $a {
                Action::RunMacro(ref name) => name == $txt,
                _ => false,
            },
            "Expected Action::RunMacro(\"{}\") but got {:?}", $txt, $a
            );
        }
    }

    macro_rules! expect_wait {
        ($a:expr, $n:expr) => {
            assert!(match $a {
                Action::Wait(n) => n == $n,
                _ => false,
            },
            "Expected Action::Wait({}) but got {:?}", $n, $a
            );
        }
    }

    macro_rules! expect_delete {
        ($a:expr, $n:expr) => {
            assert!(match $a {
                Action::Delete(n) => n == $n,
                _ => false,
            },
            "Expected Action::Delete({}) but got {:?}", $n, $a
            );
        }
    }

    macro_rules! expect_definemacro {
        ($a:expr, $name:expr, $n:expr) => {{
            match $a {
                Action::DefineMacro(ref name, ref vec) => {
                    assert!(
                        name == $name,
                        "Expected DefineMacro name to be {} but was {}", $name, name
                    );
                    assert!(
                        vec.len() == $n,
                        "Expected {} items in the macro but got {}: {:?}", $n, vec.len(), vec
                    );
                    vec
                },
                _ => panic!("Expected Action::DefineMacro({}, ...) got {:?}", $name, $a),
            }
        }}
    }

    macro_rules! expect_tab {
        ($a:expr) => {
            assert!(match $a {
                Action::Tab => true,
                _ => false,
            },
            "Expected Action::Tab but got {:?}", $a
            );
        }
    }

    macro_rules! expect_listmacros {
        ($a:expr) => {
            assert!(match $a {
                Action::ListMacros => true,
                _ => false,
            },
            "Expected Action::ListMacros but got {:?}", $a
            );
        }
    }

    macro_rules! expect_quit {
        ($a:expr) => {
            assert!(match $a {
                Action::Quit => true,
                _ => false,
            },
            "Expected Action::Quit but got {:?}", $a
            );
        }
    }

    macro_rules! expect_clear {
        ($a:expr) => {
            assert!(match $a {
                Action::Clear => true,
                _ => false,
            },
            "Expected Action::Clear but got {:?}", $a
            );
        }
    }

    macro_rules! expect_enter {
        ($a:expr) => {
            assert!(match $a {
                Action::Enter => true,
                _ => false,
            },
            "Expected Action::Enter but got {:?}", $a
            );
        }
    }

    macro_rules! expect_load {
        ($a:expr, $txt:expr) => {
            assert!(match $a {
                Action::Load(ref name) => name == $txt,
                _ => false,
            },
            "Expected Action::Load(\"{}\") but got {:?}", $txt, $a
            );
        }
    }

    macro_rules! expect_save {
        ($a:expr, $txt:expr) => {
            assert!(match $a {
                Action::Save(ref name) => name == $txt,
                _ => false,
            },
            "Expected Action::Save(\"{}\") but got {:?}", $txt, $a
            );
        }
    }

    #[test]
    fn parse_empty() {
        let buf = Vec::new();
        let actions = do_parse!(buf, 1, ok);
        assert!(match actions[0] { Action::None => true, _ => false });
    }

    fn _parse_single(s: &'static str) -> Action {
        let buf = s.as_bytes();
        let actions = do_parse!(buf, 1, ok);
        actions[0].clone()
    }

    #[test]
    fn parse_newline() {
        expect_enter!(_parse_single("\n"));
    }

    #[test]
    fn parse_tab() {
        expect_tab!(_parse_single("\t"));
    }

    #[test]
    fn parse_list_macros() {
        expect_listmacros!(_parse_single("\\macros"));
    }

    #[test]
    fn parse_clear() {
        expect_clear!(_parse_single("\\clear"));
    }

    #[test]
    fn parse_quit() {
        expect_quit!(_parse_single("\\q"));
    }

    #[test]
    fn parse_delete() {
        expect_delete!(_parse_single("\\d 10"), 10);
    }

    #[test]
    fn parse_wait() {
        expect_wait!(_parse_single("\\wait 10"), 10);
    }

    fn _parse_backspace(s: &'static str, count: u32) {
        let buf = s.as_bytes();
        let actions = do_parse!(buf, 1, ok);
        expect_delete!(actions[0], count);
    }

    #[test]
    fn parse_backspace_7f() {
        _parse_backspace("\u{007f}", 1);
    }

    #[test]
    fn parse_backspace_08() {
        _parse_backspace("\u{0008}", 1);
    }

    #[test]
    fn parse_macro_invocation_empty() {
        let buf = "\\m".as_bytes();
        do_parse!(buf, 0, err);
    }

    #[test]
    fn parse_macro_invocation() {
        let buf = "\\m foo".as_bytes();
        let actions = do_parse!(buf, 1, ok);
        expect_runmacro!(actions[0], "foo");
    }

    #[test]
    fn parse_just_text_escaped_backslash() {
        let buf = "\\\\wow text".as_bytes();
        let actions = do_parse!(buf, 1, ok);
        expect_sendtext!(actions[0], "\\wow text");
    }

    #[test]
    fn parse_load() {
        let buf = "\\load macros.json".as_bytes();
        let actions = do_parse!(buf, 1, ok);
        expect_load!(actions[0], "macros.json");
    }

    #[test]
    fn parse_save() {
        let buf = "\\save macros.json".as_bytes();
        let actions = do_parse!(buf, 1, ok);
        expect_save!(actions[0], "macros.json");
    }

    #[test]
    fn parse_multiline_with_macro() {
        let buf = "\\m foo\nsend text\n".as_bytes();
        let actions = do_parse!(buf, 3, ok);
        expect_runmacro!(actions[0], "foo");
        expect_sendtext!(actions[1], "send text");
        expect_enter!(actions[2]);
    }

    #[test]
    fn parse_multiline_with_macro_second() {
        let buf = "send text\n\\m foo\n".as_bytes();
        let actions = do_parse!(buf, 3, ok);
        expect_sendtext!(actions[0], "send text");
        expect_enter!(actions[1]);
        expect_runmacro!(actions[2], "foo");
    }

    #[test]
    fn parse_multiple_newlines() {
        let buf = "\n\n\n".as_bytes();
        let actions = do_parse!(buf, 3, ok);
        for a in actions {
            expect_enter!(a);
        }
    }

    #[test]
    fn parse_multiple_tab() {
        let buf = "\t\t\t".as_bytes();
        let actions = do_parse!(buf, 3, ok);
        for a in actions {
            expect_tab!(a);
        }
    }

    #[test]
    fn parse_multiline() {
        let buf = "\\d 20\nusername\tpassword\n".as_bytes();
        let actions = do_parse!(buf, 5, ok);
        expect_delete!(actions[0], 20);
        expect_sendtext!(actions[1], "username");
        expect_tab!(actions[2]);
        expect_sendtext!(actions[3], "password");
        expect_enter!(actions[4]);
    }

    #[test]
    fn parse_only_newline_macrodef() {
        let buf = "\\mdef test\n\n".as_bytes();
        let actions = do_parse!(buf, 1, ok);
        let inner = expect_definemacro!(actions[0], "test", 1);
        expect_enter!(inner[0]);
    }

    #[test]
    fn parse_multiple_newlines_macrodef() {
        let buf = "\\mdef test\n\n\n\n".as_bytes();
        let actions = do_parse!(buf, 1, ok);
        let inner = expect_definemacro!(actions[0], "test", 3);
        expect_enter!(inner[0]);
        expect_enter!(inner[1]);
        expect_enter!(inner[2]);
    }

    #[test]
    fn parse_just_tab_macrodef() {
        let buf = "\\mdef test\n\t".as_bytes();
        let actions = do_parse!(buf, 1, ok);
        let inner = expect_definemacro!(actions[0], "test", 1);
        expect_tab!(inner[0]);
    }

    #[test]
    fn parse_newline_and_tabs_macrodef() {
        let buf = "\\mdef test\n\t\n\t\n".as_bytes();
        let actions = do_parse!(buf, 1, ok);
        let inner = expect_definemacro!(actions[0], "test", 4);
        expect_tab!(inner[0]);
        expect_enter!(inner[1]);
        expect_tab!(inner[2]);
        expect_enter!(inner[3]);
    }

    #[test]
    fn parse_simple_macrodef() {
        let buf = "\\mdef login\nusername\tpassword\n".as_bytes();
        let actions = do_parse!(buf, 1, ok);
        let inner = expect_definemacro!(actions[0], "login", 4);
        expect_sendtext!(inner[0], "username");
        expect_tab!(inner[1]);
        expect_sendtext!(inner[2], "password");
        expect_enter!(inner[3]);
    }

    #[test]
    fn parse_bad_macrodef() {
        let buf = "\\mdef login wow great\n".as_bytes();
        do_parse!(buf, 0, err);
    }

    #[test]
    fn parse_complex_macrodef() {
        let buf = "\\mdef test\n\t\t\nhello\n\\d 6\n\n\t\nhello\t\n".as_bytes();
        let actions = do_parse!(buf, 1, ok);
        let inner = expect_definemacro!(actions[0], "test", 12);
        expect_tab!(inner[0]);
        expect_tab!(inner[1]);
        expect_enter!(inner[2]);
        expect_sendtext!(inner[3], "hello");
        expect_enter!(inner[4]);
        expect_delete!(inner[5], 6);
        expect_enter!(inner[6]);
        expect_tab!(inner[7]);
        expect_enter!(inner[8]);
        expect_sendtext!(inner[9], "hello");
        expect_tab!(inner[10]);
        expect_enter!(inner[11]);
    }

    #[test]
    fn parse_unknown_command() {
        let buf = "\\bestcommandever".as_bytes();
        do_parse!(buf, 0, err);
    }

    #[test]
    fn parse_bad_delete() {
        let buf = "\\d deadbeef".as_bytes();
        do_parse!(buf, 0, err);
    }

    #[test]
    fn parse_too_big_delete() {
        let buf = "\\d 256".as_bytes();
        do_parse!(buf, 0, err);
    }

    #[test]
    fn parse_too_big_delete_2() {
        let buf = "\\d 25613437238473".as_bytes();
        do_parse!(buf, 0, err);
    }

    #[test]
    fn parse_empty_delete() {
        let buf = "\\d".as_bytes();
        do_parse!(buf, 0, err);
    }

    #[test]
    fn parse_bad_backslash() {
        let buf = "\\".as_bytes();
        do_parse!(buf, 0, err);
    }

    #[test]
    fn parse_single_char() {
        let buf = "a".as_bytes();
        do_parse!(buf, 1, ok);
    }

    #[test]
    fn parse_multiline_bad_backslash() {
        let buf = "hi\n\\".as_bytes();
        do_parse!(buf, 2, err);
    }

    #[test]
    fn parse_long_text() {
        let extra = 5;
        let buf = "A".repeat(MAX_TEXT_BLOCK + extra);
        let actions = do_parse!(buf.as_bytes(), 2, ok);
        expect_sendtext!(actions[0], &"A".repeat(MAX_TEXT_BLOCK));
        expect_sendtext!(actions[1], &"A".repeat(extra));
    }

    #[test]
    fn parse_long_text_break_on_command() {
        let mut buf = String::from("A".repeat(MAX_TEXT_BLOCK));
        buf.push_str("\\q");
        let actions = do_parse!(buf.as_bytes(), 2, ok);
        expect_sendtext!(actions[0], &"A".repeat(MAX_TEXT_BLOCK));
        expect_sendtext!(actions[1], "\\q");
    }

    #[test]
    fn parse_long_text_newline_command() {
        let mut buf = String::from("A".repeat(MAX_TEXT_BLOCK));
        buf.push_str("\n\\q");
        let actions = do_parse!(buf.as_bytes(), 3, ok);
        expect_sendtext!(actions[0], &"A".repeat(MAX_TEXT_BLOCK));
        expect_enter!(actions[1]);
        expect_quit!(actions[2]);
    }
    /*
     * This only tests for a macro calling itself directly.
     * A macro that calls a macro that calls itself would still break this
     * and is out of scope for testing :)
     */
    #[test]
    fn no_simple_recursive_macros() {
        let buf = "\\mdef foo\n\\m foo".as_bytes();
        do_parse!(buf, 0, err);
    }
}
