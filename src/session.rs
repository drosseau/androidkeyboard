use std::{process, thread};
use std::error::Error;
use std::collections::HashMap;
use std::fs::File;
use std::path::Path;
use std::time::Duration;
use error::SimpleError;

extern crate serde_json;

macro_rules! write_str_ret {
    ($win:expr, $s:expr) => {
        if $win.addstr($s) == ERR {
            ret_err!(format!("\ncurses error writing string: {}", $s));
        }
    }
}

macro_rules! ret_err {
    ($e:expr) => {
        return Err(Box::new(SimpleError::from($e)));
    }
}

use std::io::{
    BufWriter,
    Write,
};
use std::process::{
    Command,
    Stdio,
    Child,
};

extern crate pancurses;
use pancurses::{
    ERR,
    Window,
    endwin,
};

use action::Action;

use consts::{
    KEYCODE_ENTER,
    KEYCODE_TAB,
    KEYCODE_DEL,
};

const CMD: &'static str = "adb";

#[derive(Debug)]
pub(crate) struct Session {
    pub(crate) child: Child,
    phone: Option<String>,
    macros: HashMap<String, Vec<Action>>,
}

fn start_adb(win: &Window, phone: &Option<String>) -> Child {
    let mut cmd = Command::new(CMD);
    cmd.stdin(Stdio::piped())
       .stdout(Stdio::piped())
       .stderr(Stdio::piped());

    if let Some(ref p) = *phone {
        cmd.arg("-s");
        cmd.arg(p.as_str());
    }

    cmd.arg("shell");
    let child = cmd.spawn();
    if child.is_err() {
        let e = child.err().unwrap();
        win.addstr(&format!("error starting shell: {}\n", e));
        win.addstr("Press any key to exit\n");
        win.getch();
        endwin();
        process::exit(1);
    }
    child.ok().unwrap()
}

impl Session {
    pub(crate) fn create(win: &Window, phone: &Option<String>) -> Self {
        Self{
            child: start_adb(win, phone),
            phone: phone.clone(),
            macros: HashMap::new(),
        }
    }

    pub(crate) fn recover(&mut self, win: &Window) {
        self.child = start_adb(win, &self.phone);
    }

    pub(crate) fn execute(&mut self, v: &Vec<Action>) -> Result<(), Box<Error>> {
        let mut stdin = match self.child.stdin.as_mut() {
            Some(mut s) => s,
            None => ret_err!("failed to get child stdin"),
        };
        execute(&mut stdin, v, &mut self.macros)
    }

    pub(crate) fn display_macro_names(&self, win: &Window) -> Result<(), Box<Error>> {
        for name in self.macros.keys() {
            write_str_ret!(win, &format!("\n- {}", name));
        }
        Ok(())
    }
}

fn execute<W: Write>(w: &mut W, v: &Vec<Action>, macros: &mut HashMap<String, Vec<Action>>) -> Result<(), Box<Error>> {
    for action in v {
        match *action {
            Action::Save(ref fname) => save_macros(fname, macros)?,
            Action::Load(ref fname) => load_macros(fname, macros)?,
            Action::Wait(n) => thread::sleep(Duration::from_millis(n)),
            Action::SendText(ref s) => send_text(w, s)?,
            Action::Delete(n) => send_event(w, KEYCODE_DEL, n)?,
            Action::Tab => send_event(w, KEYCODE_TAB, 1)?,
            Action::Enter => send_event(w, KEYCODE_ENTER, 1)?,
            Action::RunMacro(ref name) => {
                let entry = macros.get(name);
                match entry {
                    Some(actions) => {
                        for a in actions.iter() {
                            execute_nested(w, a, macros)?;
                        }
                    },
                    None => {
                        ret_err!(format!("macro {} not defined", name));
                    }
                };
            },
            Action::DefineMacro(ref name, ref actions) => {
                macros.insert(name.to_string(), actions.to_vec());
            },
            Action::ExportMacros(ref mac, ref fname) => export_macros(mac, fname, macros)?,
            _ => {
                ret_err!(format!("unexpected action {:?} given to session", *action));
            }
        };
    }
    Ok(())
}

fn save_macros<P: AsRef<Path>>(fname: P, macros: &HashMap<String, Vec<Action>>) -> Result<(), Box<Error>> {
    let file = File::create(fname)?;
    serde_json::to_writer(file, macros)?;
    Ok(())
}

fn export_macros<P: AsRef<Path>>(mac: &String, fname: P, macros: &HashMap<String, Vec<Action>>) -> Result<(), Box<Error>> {
    let acts = macros.get(mac);
    if acts.is_none() {
        ret_err!(format!("macro {} not defined", mac));
    }
    let mut file = File::create(fname)?;
    file.write_all("#!/bin/bash\n\n".as_bytes())?;
    for act in acts.unwrap().iter() {
        file.write_all(act.to_bash_cmd(macros)?.as_bytes())?;
    }
    Ok(())
}

fn load_macros<P: AsRef<Path>>(fname: P, macros: &mut HashMap<String, Vec<Action>>) -> Result<(), Box<Error>> {
    let file = File::open(fname)?;
    let map: HashMap<String, Vec<Action>> = serde_json::from_reader(file)?;
    for (k, v) in map.iter() {
        macros.insert(k.to_string(), v.to_vec());
    }
    Ok(())
}

fn execute_nested<W: Write>(w: &mut W, action: &Action, macros: &HashMap<String, Vec<Action>>) -> Result<(), Box<Error>> {
    match *action {
        Action::SendText(ref s) => send_text(w, s),
        Action::Delete(n) => send_event(w, KEYCODE_DEL, n),
        Action::Wait(n) => { thread::sleep(Duration::from_millis(n)); Ok(()) },
        Action::Tab => send_event(w, KEYCODE_TAB, 1),
        Action::Enter => send_event(w, KEYCODE_ENTER, 1),
        Action::RunMacro(ref name) => {
            let entry = macros.get(name);
            match entry {
                Some(actions) => {
                    for a in actions.iter() {
                        execute_nested(w, a, macros)?;
                    }
                    Ok(())
                },
                None => {
                    ret_err!(format!("macro {} not defined", name));
                }
            }
        }
        Action::DefineMacro(_, _) => {
            ret_err!("Cannot define macros inside of macros");
        },
        _ => {
            ret_err!(format!("Command {:?} not executable in nested context", action));
        }
    }
}

fn send_event<W: Write>(w: &mut W, evt: &str, count: u32) -> Result<(), Box<Error>> {
    if count == 0 {
        ret_err!("count must be >= 1 in send_event");
    }
    let mut s = String::from("input keyevent ");
    for _ in 0..count {
        s.push_str(evt);
        s.push(' ');
    }
    s.pop();
    s.push('\n');
    send_string(w, &s)
}

fn send_text<W: Write>(w: &mut W, s: &str) -> Result<(), Box<Error>> {
    let s = format!("input text '{}'\n", s);
    send_string(w, &s)
}

fn send_string<W: Write>(mut w: &mut W, s: &str) -> Result<(), Box<Error>> {
    let mut writer = BufWriter::new(&mut w);
    writer.write_all(s.as_bytes())?;
    Ok(())
}

#[cfg(test)]
mod test {
    use std::{str, fs, time};
    use parser::Action;
    use super::*;

    macro_rules! get_macros {
        () => {{
            let mut macros = HashMap::new();
            macros.insert(
                String::from("simple"),
                vec![
                    Action::Tab,
                    Action::Enter,
                ]
            );
            macros.insert(
                String::from("all_allowed"),
                vec![
                    Action::Delete(2),
                    Action::SendText(String::from("test")),
                    Action::RunMacro(String::from("simple")),
                ]
            );
            macros.insert(
                String::from("bad_one"),
                vec![
                    Action::DefineMacro(
                        String::from("nested"),
                        Vec::new()
                    )
                ]
            );
            macros
        }}
    }

    macro_rules! check_val {
        ($buf:expr, $exp:expr) => {
            match str::from_utf8(&$buf) {
                Err(e) => panic!("Failed to get buf {:?} as utf8 string: {:?}", $buf, e),
                Ok(s) => assert!(
                    String::from(s) == String::from($exp),
                    "buffer didn't have expected string `{}` got `{}`", $exp, s
                ),
            }
        }
    }

    macro_rules! is_ok {
        ($r:expr) => { assert!($r.is_ok(), "Expected Ok but got {:?}", $r) };
    }
    macro_rules! is_err {
        ($r:expr) => { assert!($r.is_err(), "Expected Err but got {:?}", $r) };
    }

    macro_rules! expect_sendtext {
        ($a:expr, $txt:expr) => {
            assert!(match $a {
                Action::SendText(ref t) => t == $txt,
                _ => false,
            },
            "Expected Action::SendText(\"{}\") but got {:?}", $txt, $a
            );
        }
    }

    macro_rules! expect_tab {
        ($a:expr) => {
            assert!(match $a {
                Action::Tab => true,
                _ => false,
            },
            "Expected Action::Tab but got {:?}", $a
            );
        }
    }

    macro_rules! expect_enter {
        ($a:expr) => {
            assert!(match $a {
                Action::Enter => true,
                _ => false,
            },
            "Expected Action::Enter but got {:?}", $a
            );
        }
    }

    #[test]
    fn session_send_string() {
        let v = "test";
        let mut buf = Vec::with_capacity(8);
        let r = send_string(&mut buf, &v);
        is_ok!(r);
        check_val!(buf, v);
    }

    #[test]
    fn session_send_text() {
        let v = "test";
        let mut buf = Vec::with_capacity(64);
        let r = send_text(&mut buf, v);
        is_ok!(r);
        check_val!(buf, "input text 'test'\n");
    }

    macro_rules! send_actions {
        ($exp:expr, $($action:expr),+) => {{
            let mut action = Vec::new();
            $(
                action.push($action);
            )*
            let mut buf = Vec::with_capacity(64);
            let mut macros = get_macros!();
            let r = execute(&mut buf, &action, &mut macros);
            is_ok!(r);
            check_val!(buf, $exp);
            (buf, macros)
        }};
    }

    macro_rules! fail_send_actions {
        ($exp:expr, $($action:expr),+) => {{
            let mut action = Vec::new();
            $(
                action.push($action);
            )*
            let mut buf = Vec::with_capacity(64);
            let mut macros = get_macros!();
            let r = execute(&mut buf, &action, &mut macros);
            is_err!(r);
            check_val!(buf, $exp);
            (buf, macros)
        }}
    }

    #[test]
    fn session_send_delete() {
        send_actions!("input keyevent 67 67\n", Action::Delete(2));
    }

    #[test]
    fn session_send_tab() {
        send_actions!("input keyevent 61\n", Action::Tab);
    }

    #[test]
    fn session_send_enter() {
        send_actions!("input keyevent 66\n", Action::Enter);
    }

    #[test]
    fn session_send_multiple() {
        send_actions!(
            "input text 'hi'\ninput keyevent 66\n",
            Action::SendText(String::from("hi")), Action::Enter
        );
    }

    #[test]
    fn session_do_macro() {
        send_actions!(
            "input keyevent 61\ninput keyevent 66\n",
            Action::RunMacro(String::from("simple"))
        );
    }

    #[test]
    fn session_unknown_macro() {
        fail_send_actions!(
            "",
            Action::RunMacro(String::from("doesntexist"))
        );
    }

    #[test]
    fn session_bad_event() {
        fail_send_actions!(
            "",
            Action::Delete(0)
        );
    }

    #[test]
    fn session_nested() {
        send_actions!(
            "input keyevent 67 67\ninput text 'test'\ninput keyevent 61\ninput keyevent 66\n",
            Action::RunMacro(String::from("all_allowed"))
        );
    }

    #[test]
    fn session_no_nested_defines() {
        fail_send_actions!(
            "",
            Action::RunMacro(String::from("bad_one"))
        );
    }

    #[test]
    fn session_wait() {
        let t = 50u64;
        let dur = time::Duration::from_millis(t);
        let action = vec![Action::Wait(t)];
        let mut buf = Vec::with_capacity(64);
        let mut macros = get_macros!();
        let now = time::Instant::now();
        let r = execute(&mut buf, &action, &mut macros);
        let done = now.elapsed();
        is_ok!(r);
        assert!(done >= dur);
    }

    #[test]
    #[allow(unused_must_use)] // ignore deleting the file
    fn session_save_load_macros() {
        let fname = String::from("test-0aaaf35cc22e5c694005.json");
        send_actions!(
            "",
            Action::Save(fname.clone())
        );
        let mut buf = Vec::with_capacity(64);
        let mut macros = HashMap::new();
        let actions = vec![Action::Load(fname.clone())];
        let r = execute(&mut buf, &actions, &mut macros);
        is_ok!(r);
        check_val!(buf, "");
        let key = String::from("simple");
        assert!(
            macros.contains_key(&key),
            "macro simple wasn't loaded: {:?}", macros
        );
        let loaded = macros.get(&key);
        assert!(
            loaded.is_some(),
            "Couldn't get actions for {}: {:?}", key, loaded
        );
        expect_tab!(loaded.unwrap()[0]);
        expect_enter!(loaded.unwrap()[1]);
        fs::remove_file(&fname);
    }

    #[test]
    fn session_define_macro() {
        let key = String::from("test");
        let (_, macros) = send_actions!(
            "",
            Action::DefineMacro(
                key.clone(),
                vec![Action::SendText(String::from("hello")), Action::Enter]
            )
        );
        assert!(
            macros.contains_key(&key),
            "macro test wasn't defined: {:?}", macros
        );
        let actions = macros.get(&key);
        assert!(
            actions.is_some(),
            "Couldn't get actions for {}: {:?}", key, actions
        );
        expect_sendtext!(actions.unwrap()[0], "hello");
        expect_enter!(actions.unwrap()[1]);
    }
}
