use std::{fmt};
use std::error::Error;

#[derive(Debug)]
pub struct SimpleError {
    desc: String,
}

impl From<&'static str> for SimpleError {
    fn from(s: &'static str) -> Self {
        Self{
            desc: String::from(s),
        }
    }
}

impl From<char> for SimpleError {
    fn from(c: char) -> Self {
        Self{
            desc: format!("\nerror writing char {}", c),
        }
    }
}

impl From<String> for SimpleError {
    fn from(s: String) -> Self {
        Self{
            desc: s,
        }
    }
}

impl fmt::Display for SimpleError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.desc)
    }
}

impl Error for SimpleError {
    fn description(&self) -> &str {
        return self.desc.as_str()
    }

    fn cause(&self) -> Option<&Error> {
        return None
    }
}

