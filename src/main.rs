mod parser;
mod session;
mod editor;
mod action;
mod consts;

#[macro_use]
mod error;

use std::io::Read;
use std::time::Duration;
use std::error::Error;
use std::{env, process, str, thread};

use action::Action;
use parser::Parser;
use editor::Editor;
use session::Session;

#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;

extern crate pancurses;

use pancurses::{
    Window,
    initscr,
    noecho,
    endwin,
};

macro_rules! exit_on_error {
    ($win:expr, $e:expr, $child:expr) => {
        $win.addstr("\nerror: ");
        $win.addstr($e.description());
        $win.addch('\n');
        if let Some(e) = $child.kill().err() {
            $win.addstr(&format!("Error killing adb process: {}\n", e.description()));
        } else {
            if let Some(e) = $child.wait().err() {
                $win.addstr(
                    &format!(
                        "Error waiting on child process: {}\n",
                        e.description()
                    )
                );
            }
        }
        $win.addstr("Press any key to exit\n");
        $win.getch();
        endwin();
        process::exit(1);
    }
}

fn clear_window(win: &Window, start_y: &i32) {
    let y = win.get_cur_y();
    for i in (*start_y..y+1).rev() {
        win.mv(i, 0);
        win.clrtoeol();
    }
}

fn main() {
    let mut args = env::args();
    let phone = args.nth(1);
    let window = initscr();
    window.refresh();
    window.scrollok(true);
    noecho();
    let mut session  = Session::create(&window, &phone);
    let mut editor = Editor::new(&window, &phone);
    editor.write_banner();
    let start_y = window.get_cur_y();
    let mut bad_exit = false;
    loop {
        match session.child.try_wait() {
            Ok(Some(_)) => {
                // We can try to recover here and if it exits again then leave
                session = Session::create(&window, &phone);
                thread::sleep(Duration::from_millis(500));
                match session.child.try_wait() {
                    Ok(Some(_)) => {
                        bad_exit = true;
                        break;
                    },
                    Err(e) => {
                        window.addstr(&format!("error checking process: {}\n", e));
                        bad_exit = true;
                        break;
                    },
                    _ => {
                        session.recover(&window);
                        window.addstr(
                            "\nWARNING: session was disconnected but recovered\n"
                        );
                    },
                };
            },
            Err(e) => {
                window.addstr(&format!("error checking process: {}\n", e));
                bad_exit = true;
                break;
            },
            _ => (),
        };

        let buf = match editor.get_line() {
            Ok(buf) => buf,
            Err(e) => {
                exit_on_error!(&window, e, session.child);
            }
        };

        let mut actions = Vec::new();
        let mut parser = Parser::new(&buf);
        if let Some(e) = parser.parse(&mut actions).err() {
            window.addstr(&format!("\nparse error: {}", e.description()));
            continue;
        };
        if actions.len() == 1 {
            match actions[0] {
                Action::None => {
                    // hmm
                    continue;
                },
                Action::Quit => {
                    break;
                },
                Action::Clear => {
                    clear_window(&window, &start_y);
                    continue;
                }
                Action::Secret => {
                    editor.toggle_secret();
                    continue;
                },
                Action::ListMacros => {
                    if let Some(e) = session.display_macro_names(&window).err() {
                        window.addstr(&format!("\nfailed to display macros: {}\n", e.description()));
                    }
                    continue;
                },
                // Fall through to execute
                _ => {},
            }
        }
        if let Some(e) = session.execute(&actions).err() {
            window.addstr(&format!("\nerror: {}", e.description()));
        }
    }
    if !bad_exit {
        if let Some(e) = session.child.kill().err() {
            window.addstr(&format!("error killing child {}\n", e.description()));
        } else {
            if let Some(e) = session.child.wait().err() {
                window.addstr(&format!("error waiting on child {}\n", e.description()));
            }
        }
    } else {
        editor.clear_line();
        window.addstr("adb exited unexpectedly:\n");
        let ostderr = session.child.stderr.as_mut();
        if ostderr.is_none() {
            window.addstr("failed to get child stderr\n");
            window.addstr("Press any key to exit\n");
            window.getch();
            endwin();
            process::exit(1);
        }

        let reader = ostderr.unwrap();
        let mut into = [0u8; 1024];
        loop {
            match reader.read(&mut into) {
                Ok(n) => {
                    if n != 0 {
                        match str::from_utf8(&into[0..n]) {
                            Ok(s) => {
                                window.addstr(&format!("{}", s));
                            },
                            Err(e) => {
                                window.addstr(&format!("{}", e));
                                break;
                            }
                        };
                    } else {
                        break;
                    }
                }
                Err(e) => {
                    window.addstr(&format!("Error reading stderr {}", e));
                    break;
                }
            }
        }
        window.addstr("\nPress any key to exit\n");
        window.getch();
    }
    endwin();
}
