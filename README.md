[![Build Status](https://travis-ci.org/drosseau/androidkeyboard.svg?branch=master)](https://travis-ci.org/drosseau/androidkeyboard)

# androidkeyboard

Write to your Android phone using adb input commands (ie `[adb shell] input text` and `[adb shell] input keyevent`). Note that due to limitations with `adb shell input text` unicode characters are currently not supported. There is a plan to create an accompanying app to deal with this as well as retrieving the clipboard.

## Installing

Install with Cargo, https://doc.rust-lang.org/cargo/getting-started/installation.html, by running `cargo install` in the root dir. This will put the binary `androidkeyboard` in your `$HOME/.cargo/bin/` directory (this needs to be in your `PATH`).

This is tested and used on macOS. It should work fine on Linux and the CI tests that it builds. Windows has been tested in a VM and seems to work, but the author doesn't use Windows often so YMMV.

## Usage

The phone needs to be plugged in and allow adb. If you have multiple devices you can put the ID after the command and it will run specifying that device (`androidkeyboard [deviceId]`).

To enable/disable multiline mode hit F1. Multiline mode is mostly useful for defining macros.

See [the wiki](https://github.com/drosseau/androidkeyboard/wiki) for detailed information about the editor itself, commands, and macros.

![Text in program](programscreen.png)
![Text sent to phone](phonescreen.png)
